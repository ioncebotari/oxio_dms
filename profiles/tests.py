from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

import re
from profiles.views import home_page,profile, add_key
from profiles import models

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        """ The home page view must resolve to the home_page view function """
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        """ The home page view must return correct HTML code """
        request = HttpRequest()
        response = home_page(request).content.decode()
        expected_response = render(request,'home.html').content.decode()
        self.assertEqual(response, expected_response)

class UserAuthTemplateTest(TestCase):

    def test_login_page_uses_correct_template(self):
        """ The login view must use the registration/login.html template """
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response,'registration/login.html')

    def test_logout_page_uses_correct_template(self):
        """ The logout view must use the registration/logout.html template """
        response = self.client.get('/accounts/logout/')
        self.assertTemplateUsed(response,'registration/logged_out.html')

    # Test temporarily disabled until understanding how the shit works
    #def test_password_change_page_uses_correct_template(self):
    #    response = self.client.get('/accounts/password_change/')
    #    self.assertTemplateUsed(response,'registration/password_change.html')

    #def test_password_change_page_uses_correct_template(self):
    #    """ The password change view must use the registration/password_changed.html view """
    #    response = self.client.get('/accounts/password_changed/')
    #    self.assertTemplateUsed(response,'registration/password_changed.html')

    def test_password_reset_page_uses_correct_template(self):
        """ The password reset view must use the registration/password_reset_form.html template """
        response = self.client.get('/accounts/password_reset/')
        self.assertTemplateUsed(response,'registration/password_reset_form.html')

    def test_password_reset_done_page_uses_correct_template(self):
        """ The password reset done view must use the registration/password_reset_done.html template """
        response = self.client.get('/accounts/password_reset/done/')
        self.assertTemplateUsed(response,'registration/password_reset_done.html')


class UserAuthSignUpTest(TestCase):

    def test_signup_page_uses_correct_template(self):
        """ The sign-up view must use the registration/login.html view """
        response = self.client.get('/accounts/signup/')
        self.assertTemplateUsed(response,'signup.html')

    def test_saving_a_POST_request(self):
        """ The signup page must properly save a correct POST request and create a new user """
        response=self.client.post(
            '/accounts/signup/',
            {
                 'username':'test1',
                 'password1':'test1test1',
                 'password2':'test1test1'
            }
        )

        self.assertEqual(User.objects.count(),1)
        new_user = User.objects.first()
        self.assertEqual(new_user.username,'test1')

    def test_redirects_after_POST(self):
        """ After a new user is created, he must be redirected to login page """
        response = self.client.post(
            '/accounts/signup/',
            {
                 'username':'test1',
                 'password1':'test1test1',
                 'password2':'test1test1'
            }
        )

        self.assertRedirects(response,'/accounts/login/')

class ProfilePageTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('test1','test1@test1.com','test1test1')

    def test_profile_url_resolves_to_profile_page_view(self):
        """ Checks that the profile URL is properly resolved to the correct view """
        found = resolve('/accounts/profile/')
        self.assertEqual(found.func, profile)

    def test_accessing_profile_page_unathenticated(self):
        """ An anonimous user can't access this page. 
        He will be automatically redirected to the login page """
        response = self.client.get('/accounts/profile/')
        self.assertRedirects(response,'/accounts/login/')
        self.client.login(username = 'test1',password = 'test1test1')
        response = self.client.get('/accounts/profile/')
        self.assertEqual(response.status_code,200)

class KeyTest(TestCase):

    def setUp(self):
        self.user1 = User.objects.create_user('test1','test1@test1.com','test1test1')
        self.user2 = User.objects.create_user('test2','test2@test2.com','test2test2')
        self.key1 = models.Key()
        self.key2 = models.Key()
        self.key1.key_name = 'Test Key1'
        self.key2.key_name = 'Test Key2'
        self.key1.key_contents = 'Test Contents1'
        self.key2.key_contents = 'Test Contents2'

    def test_add_key_page_redirects_unauthenticated_users(self):
        """ An anonimous user can't access this page.
        He will be automatically redirected to the login page """
        response = self.client.get('/keys/')
        self.assertRedirects(response,'/accounts/login/')

    def test_key_add_url_resolves_to_add_key_view(self):
        """ The Add Key URL must be properly resolved to the correct view """
        found = resolve('/keys/')
        self.assertEqual(found.func, add_key)

    def test_add_key_page_generates_the_form(self):
        """ The fields "key name", "key contents" and the "add key" button
        must be present in the generated form """
        self.client.login(username = 'test1',password = 'test1test1')
        response = self.client.get('/keys/')
        self.assertContains(response,'Key name')
        self.assertContains(response,'Key contents')
        self.assertContains(response,'Add Key')

    def test_saved_keys_are_present_on_page(self):
        """ After being saved, the keys must be present
        on the current page """
        self.client.login(username = 'test1',password = 'test1test1')
        self.key1.key_user = self.user1
        self.key2.key_user = self.user1
        self.key1.save()
        self.key2.save()
        response = self.client.get('/keys/')
        self.assertContains(response,'Test Key1')
        self.assertContains(response,'Test Key2')

    def test_user1_cant_delete_user2_keys(self):
        """ A user should be able to delete his own keys,
        with a redirect to keys page. If anyone tries removing a key
        that doesn't belong to him, he gets a 403 error """
        self.client.login(username = 'test1',password = 'test1test1')
        self.key1.key_user = self.user1
        self.key2.key_user = self.user2
        self.key1.save()
        self.key2.save()

        response1 = self.client.get('/keys/delete/'+str(self.key1.pk)+'/')
        response2 = self.client.get('/keys/delete/'+str(self.key2.pk)+'/')

        self.assertRedirects(response1,'/keys/')
        self.assertEqual(response2.status_code,403)
